<?php

require_once ('animal.php');
require_once ('ape.php');
require_once ('frog.php');

$sheep = new Animal("shaun");

echo "name : $sheep->name <br>";// "shaun"
echo "legs : $sheep->legs <br>";// 4
echo "cold_blooded : $sheep->cold_blooded <p>";// "no"

$kodok = new Frog("buduk");

echo "name : $kodok->name <br>";// "shaun"
echo "legs : $kodok->legs <br>";// 4
echo "cold_blooded : $kodok->cold_blooded <br>";
$kodok->jump() ; // "hop hop"

$sungokong = new Ape("kera sakti");

echo "name : $sungokong->name <br>";// "shaun"
echo "legs : $sungokong->legs <br>";// 4
echo "cold_blooded : $sungokong->cold_blooded <br>";
$sungokong->yell(); // "Auooo"